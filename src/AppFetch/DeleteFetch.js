/**
 * fetch the todoElement and make a delete of him into the Application and Database.
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @param value {Object} TodoElement.
 * @param setTab {function} React Hook setState function used to delete the element directly into react dom after fetch response : 204. look at : {@link setTabTodo}
 * @param tab {Object} Array of actual TodoElement in the tabTodo {@link tabTodo}
 * @constructor
 */
function DeleteFetch(value, setTab , tab) {
    /**
     * Set the init for the fetch DELETE todoElement.
     * @type {{headers: {Accept: string, "Content-Type": string}, method: string}}
     * @example {
        "method": 'DELETE',
        "headers": {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
    }
     */
    let init = {
        "method": 'DELETE',
        "headers": {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
    };
    /**
     * fetch the Delete.
     */
    fetch("http://127.0.0.1:8000" + value["@id"], init)
        /**
         * Take response status
         */
        .then(res => res["status"])
        .then((data) => {
            /**
             * if the element was Deleted
             */
            if(data === 204){
                let tabAfterDelete = tab.slice().filter(tabval => tabval.id !== value.id );
                setTab(tabAfterDelete);
            }
            /**
             * else throw an error
             */
            else{
                throw new Error("Objet inexistant");
            }
        })
        /**
         * catch all error
         */
        .catch(console.log);
}

export default DeleteFetch;