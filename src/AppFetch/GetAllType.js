/**
 * Fetch all typeElement from the Database with a GET to /api/types.
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @param setTabType {function} React Hook setState function used to init the TypeElement Array. look at : {@link setTabType}
 * @constructor
 */
function GetAllType(setTabType, setStart) {
    fetch("http://127.0.0.1:8000/api/types")
        .then(res => {
            if (res.ok) {
                return res.json()
            }
            else {
                console.log("coucou")
            }
        })
        .then((data) => {
            setTabType(data["hydra:member"]);
            setStart(true);
        })
        .catch(
            (error) => {
                setStart(null);
            }
        );
}

export default GetAllType;
