/**
 * Fetch into Database a new TodoElement.
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @param formPost {Object} Values of {@link TypeFormAdd}
 * @param setTabType {function} React Hook setState function used to add the element directly into react dom after fetch response data good Type. look at : {@link setTabType}
 * @param tabType {Object} Array of actual TodoElement in the tabTodo {@link tabType}
 * @constructor
 */
function AddType(formPost,tabType,setTabType) {
    /**
     * Use to get all input from formPost {@link TypeFormAdd}
     * @type {HTMLCollectionOf<HTMLInputElement>}
     */
    let tabInput = formPost.getElementsByTagName('input');
    /**
     * Use to make an Array "key":"value" , with values from tabInput.
     * @type {{}}
     * @example for (let i = 0; i < tabInput.length ; i++) {
        tabOutput[tabInput[i].name]=tabInput[i].value;
    }
     * @example tabOutput look like : {"titre":"Hello World i am a Title","description":"i am an hello world description"}
     */
    let tabOutput = {};
    for (let i = 0; i < tabInput.length ; i++) {
        tabOutput[tabInput[i].name]=tabInput[i].value;
    }
    /**
     * Convert tabOutput into Json.
     * @type {string}
     */
    let datajson = JSON.stringify(tabOutput);
    /**
     * Set the init for the fetch POST todoElement.
     * @type {{headers: {Accept: string, "Content-Type": string}, method: string, body: string}}
     * @example {
        "method": 'POST',
        "headers":{
            "Accept":"application/ld+json",
            "Content-Type":"application/ld+json; charset=utf-8",
        },
        "body": datajson,
    }
     */
    let init = {
        "method": 'POST',
        "headers":{
            "Accept":"application/ld+json",
            "Content-Type":"application/ld+json; charset=utf-8",
        },
        "body": datajson,
    };
    /**
     * fetch to the Api the new TodoElement
     */
    fetch("http://127.0.0.1:8000/api/types", init)
        .then((res=>res.json()))
        .then(
            (data)=> {
                data["@type"] === "Type" ?setTabType([...tabType,data]):console.log("erreur");
            }
        )
        .catch(console.log);
}

export default AddType;
