/**
 * Fetch the modified todoElement and modified him into the dataBase.
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @param value {Object} TodoElement.
 * @param setTab {function} React Hook setState function used to Modify the element directly into react dom after fetch response data good Type. look at : {@link setTabTodo}
 * @param tab {Object} Array of actual TodoElement in the tabTodo {@link tabTodo}
 * @constructor
 */
function ModifyFetch(value, tab, setTab,type) {
    /**
     * Used to fetch data with Json format.
     * @type {string}
     */
    let dataJson = JSON.stringify(value);
    /**
     * Set the init for the fetch PUT todoElement.
     * @type {{headers: {Accept: string, "Content-Type": string}, method: string, body: string}}
     * @example {
        "method": 'PUT',
        "headers": {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        "body": dataJson,
     */
    let init = {
        "method": "PUT",
        "headers": {
            "Accept": "application/ld+json",
            "Content-Type": "application/ld+json"
        },
        "body": dataJson,
    };
    /**
     * fetch the modification into the database
     */
    fetch("http://127.0.0.1:8000" + value["@id"], init)
        /**
         * take the response
         */
        .then(res => res.json())
        /**
         * use it to change the local state of {@link tabTodo}.
         */
        .then((data) => {
            /**
             * Make copy, modified it.
             */
            let toast = tab.map(element => (element.id === value.id ? {...element,value} : element));
            /**
             * if all right, push else ...
             */
            data["@type"] === type ? setTab(toast) : console.log("error");
        })
        .catch(console.log);
}

export default ModifyFetch;