/**
 * Fetch all todoElement from the Database with a GET to /api/todos.
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @param setTabTodo {function} React Hook setState function used to init the TodoElement Array. look at : {@link setTabTodo}
 * @param setStart {function} React Hook setState function used to change Start state use to init the TodoElement Array. look at : {@link setStart}
 * @param mode {string} Use to know if we use an type or not.
 * @constructor
 */
function GetAllTodo(setTabTodo, setStart, mode = false) {
    let TodoFetch = mode ? fetch("http://127.0.0.1:8000" + mode + "/todos") : fetch("http://127.0.0.1:8000/api/todos");
    TodoFetch.then(res => {
        switch (res.status) {
            case 200:
                return res.json();
                break;
            case 404:
                throw new Error("404 page not found");
                break;
            default:
                console.log(res.status);
                break;
        }
    })
        .then((data) => {
            setTabTodo(data["hydra:member"]);
        })
        .catch(
            (error) => {
                console.log(error);
                setStart(false);
            }
        );
}

export default GetAllTodo;
