import React, {useEffect, useState} from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import GetAllTodo from "./AppFetch/GetAllTodo";
import TodoFormAdd from "./AppAff/TodoFormAdd";
import TodoAffich from "./AppAff/TodoAffich";
import GetAllType from "./AppFetch/GetAllType";
import TypeSelect from "./AppAff/TypeSelectAff";
import TypeFormAdd from "./AppAff/TypeFormAdd";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import TypeManage from "./AppAff/TypeManage";

/**
 * Main function of the Application
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @todo finish documentation
 * @returns {*}
 * @constructor
 */
function App() {
    /**
     * @const {{}} tabTodo Array of all TodoElement.
     * @example {}
     */
    /**
     * @const {function} setTabTodo used to change the state of {@link tabTodo}
     */
    const [tabTodo, setTabTodo] = useState([]);
    /**
     * @const {boolen} start used to init the first charge of {@link tabTodo} with {@link GetAllTodo}
     */

    /**
     * @const {function} setStart used to change the state of {@link start}
     */
    const [start, setStart] = useState(null);

    const [tabType, setTabType] = useState([]);

    const [actualType, setActualType] = useState('');

    const [addTodoForm, setAddTodoForm] = useState(false);

    const [manageForm, setManageForm] = useState(false);

    const [modifyType, setModifyType] = useState(false);
    useEffect(() => {
        GetAllTodo(setTabTodo, setStart);
        GetAllType(setTabType, setStart);
    }, [start]);
    if (start) {
        return (
            <div>
                <CssBaseline/>

                <Grid
                    container
                    direction={"column"}
                    alignItems={"center"}
                >
                    <TypeSelect
                        actualType={actualType}
                        setActualType={setActualType}
                        setTabTodo={setTabTodo}
                        tabType={tabType}
                        setStart={setStart}
                        setManageForm={setManageForm}
                        manageForm={manageForm}
                        setAddTodoForm={setAddTodoForm}
                        setModifyType={setModifyType}/>
                    <TypeManage
                        actualType={actualType}
                        manageForm={manageForm}
                        setAddTodoForm={setAddTodoForm}
                        addTodoForm={addTodoForm}
                        setTabType={setTabType}
                        tabType={tabType}
                        modifyType={modifyType}
                        setModifyType={setModifyType}
                    />
                    <TodoFormAdd tabTodo={tabTodo} setTabTodo={setTabTodo} actualType={actualType}
                                 addTodoForm={addTodoForm}/>
                    <TypeFormAdd actualType={actualType} setTabType={setTabType} tabType={tabType} modifyType={modifyType}/>
                    <TodoAffich tabTodo={tabTodo} setTabTodo={setTabTodo}/>
                </Grid>
            </div>
        );
    } else {
        return <p>Chargement en Cours</p>
    }
}

export default App;
