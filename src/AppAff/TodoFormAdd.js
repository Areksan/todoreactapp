import AddTodo from "../AppFetch/TodoAdd";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import React from "react";

/**
 * Form to Add Element in the TodoList
 * @author Heynderickx Alexandre <heynderickxalexandre@gmail.com>
 * @returns {HTMLFormElement} This form was used to add new TodoElement in the DataBase.
 * @constructor
 */
function TodoFormAdd(props) {
    if (props.addTodoForm) {
        return (
            <form className={"initForm"} onSubmit={(e) => {
                /**
                 * @type {EventTarget}
                 */
                let target = e.target;
                /**
                 * Use To Add values of the form into new TodoElement in DataBase .
                 * @see AddTodo
                 */
                AddTodo(target, props.tabTodo, props.setTabTodo);
                /**
                 * Set to Null the Textfield named : titre
                 * @type {string}
                 */
                target.titre.value = "";
                /**
                 * Set to Null the Textfield named : description.
                 * @type {string}
                 */
                target.description.value = "";
                target.date.value = "";
                target.time.value = "";
                e.preventDefault();
            }}>
                <div>
                    <TextField variant={"outlined"} label={"Titre"} name={"titre"}/>
                </div>
                <div>
                    <TextField variant={"outlined"} label={"Faire"} name={"description"}/>
                </div>
                <div>
                    <TextField
                        name="date"
                        label="Date"
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </div>
                <div>
                    <TextField
                        name="time"
                        label="Heure"
                        type="time"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </div>
                <input type={"hidden"} name={"type"} value={props.actualType["@id"]}/>
                <div>
                    <Button variant={"contained"} color={"primary"} type={"submit"}>Envoyer</Button>
                </div>
            </form>
        );
    } else {
        return null;
    }
}

export default TodoFormAdd;
