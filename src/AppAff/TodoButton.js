import React, {useState} from "react";
import ModifyFetch from "../AppFetch/ModifyFetch";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import DeleteFetch from "../AppFetch/DeleteFetch";

function TodoButton(props) {
    const [textInput, setTextInput] = useState(false);
    const x = (
            <form className={"modbar"} onSubmit={
                (e) => {
                    let todoObject = props.todoObject;
                    todoObject.titre = e.target.title.value;
                    todoObject.description = e.target.text.value;
                    ModifyFetch(todoObject, props.tabTodo, props.setTabTodo,"Todo");
                    e.target.text.value = "";
                    e.target.title.value = "";
                    setTextInput(false);
                    e.preventDefault();
                }
            }>
                <div>
                    <TextField name={"title"} label={"Titre"} variant={"outlined"}/>
                </div>
                <div>
                    <TextField name={"text"} label={"Description"} variant={"outlined"}/>
                </div>
                <Button variant={"contained"} type={"submit"}>Valider</Button>
            </form>
        )
    ;
    return (
        <div>
            <ButtonGroup variant={"contained"}>
                <Button color={"primary"} onClick=
                    {(e) => {
                        setTextInput(!textInput);
                    }}>
                    modifier
                </Button>
                <Button onClick=
                            {(e) => {
                                DeleteFetch(props.todoObject, props.setTabTodo, props.tabTodo);
                            }}>
                    supprimer
                </Button>
            </ButtonGroup>
            {textInput ? x : null}
        </div>
    );
}

export default TodoButton;
