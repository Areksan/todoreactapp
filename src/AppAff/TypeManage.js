import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import React from "react";
import DeleteFetch from "../AppFetch/DeleteFetch";

function TypeManage(props) {
    if(props.manageForm){
        return (
            <Tabs
                value={0}>
                <Tab label={props.actualType.name} disabled/>
                <Tab label={props.addTodoForm ? "Close":"Add Todo"} onClick={(e)=>{
                    props.setModifyType(false);
                    props.setAddTodoForm(!props.addTodoForm);
                }}/>
                <Tab label={props.modifyType ? "Close":"Modify Type"} onClick={(e)=>{
                    props.setAddTodoForm(false);
                    props.setModifyType(!props.modifyType);
                }} />
                <Tab label={"Delete Old Todo"} disabled/>
                <Tab label={"Delete Type"} onClick={(e)=>{
                    DeleteFetch(props.actualType,props.setTabType,props.tabType);
                }} />
            </Tabs>
        );
    } else {
        return null;
    }
}

export default TypeManage;
