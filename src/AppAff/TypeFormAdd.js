import AddType from "../AppFetch/TypeAdd";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import React from "react";
import ModifyFetch from "../AppFetch/ModifyFetch";

/**
 * @return {null}
 */
function TypeFormAdd(props) {
    if (props.actualType === "new" || props.modifyType === true){
        return (
            <form className={"initForm"} onSubmit={(e) => {
                /**
                 * @type {EventTarget}
                 */
                let target = e.target;
                /**
                 * Use To Add values of the form into new TodoElement in DataBase .
                 * @see AddTodo
                 */
                if(props.actualType === "new") {
                    AddType(target, props.tabType, props.setTabType);
                } else {
                    let modify = props.actualType;
                    modify.name= target.name.value;
                    ModifyFetch(modify,props.tabType,props.setTabType,"Type");
                }
                /**
                 * Set to Null the Textfield named : titre
                 * @type {string}
                 */
                target.name.value = "";
                e.preventDefault();
            }}>
                <div>
                    <TextField variant={"outlined"} label={"Name"} name={"name"}/>
                </div>
                <div>
                    <Button variant={"contained"} color={"primary"} type={"submit"}>Envoyer</Button>
                </div>
            </form>
        )
    } else {
        return null;
    }
}

export default TypeFormAdd;
