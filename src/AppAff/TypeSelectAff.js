import React, {useEffect, useState} from "react";
import Tab from "@material-ui/core/Tab";
import GetAllTodo from "../AppFetch/GetAllTodo";
import Tabs from "@material-ui/core/Tabs";

function TypeSelect(props){
    const [actual,setActual] = useState(0);
    let menuLi = props.tabType.map((value,key) => (
        <Tab key={key +1} onClick={(e)=>
        {
            if (!props.manageForm){
                props.setManageForm(true);
            }
            GetAllTodo(props.setTabTodo,props.setStart, value["@id"]);
            props.setActualType(value);
            setActual(key+1);
        }
        }
             label={value.name}>
        </Tab>
    ));
    return (
        <Tabs
            value={actual}
            indicatorColor={"primary"}
            textColor={"secondary"}>
            <Tab key={0} onClick={()=>
            {
                ManageFalse([props.setManageForm,props.setAddTodoForm,props.setModifyType]);
                GetAllTodo(props.setTabTodo,props.setStart);
                props.setActualType("");
                setActual(0);
            }
            }
                 label={"All Todo"}>
            </Tab>
            {menuLi}
            <Tab
                onClick={()=>{
                    ManageFalse([props.setManageForm,props.setAddTodoForm,props.setModifyType]);
                    props.setTabTodo([]);
                    props.setActualType("new");
                    setActual(props.tabType.length+1);
                }}
                label={"New Type"}
                // disabled
            >
            </Tab>
        </Tabs>
    );
}

function ManageFalse(tabTofalse){
    for (let i = 0; i < tabTofalse.length ; i++) {
        tabTofalse[i](false);
    }
}
export default TypeSelect;
