import React from "react";
import TodoButton from "./TodoButton";

function TodoAffich(props) {
    return props.tabTodo.map((value) => (
        <fieldset className={"Todo"} key={value.id}>
            <legend>{value.titre}</legend>
            <div>{value.description}</div>
            <div>Pour Le : {new Date(value.date).toLocaleDateString()}</div>
            <div>A : {new Date(value.time).toLocaleTimeString()}</div>

            <TodoButton
                todoObject={value}
                setTabTodo={props.setTabTodo}
                tabTodo={props.tabTodo}
            />
        </fieldset>
    ));
}

export default TodoAffich;
